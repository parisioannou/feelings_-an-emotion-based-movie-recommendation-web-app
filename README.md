In this project I have trained an emotion classification model using deep learning techniques (CNNs, Transfer Learning, Cyclical Learning Rate). Then using a webcam users are able to take pictures of themselves. The image then runs through face detection and the emotion classifier, which finally prints the emotion and the detected face on the taken image. The predicted emotion is used as input to a custom recommendation algorithm, which suggests movie titles according to the emotion (from a movies dataset). The website and deployment have been implemented using the Django platform, a Python web framework. UI using HTML, CSS, Bootstrap.



To run the application:

1) Install all dependencies in requirements.txt.
2)In Command Window:
	a) activate environment where dependencies have been isntalled into.
	b) go into the project folder (djnago)
	c) run the command: python manage.py runserver

Make sure your camera is connected.
Visit URL: http://127.0.0.1:8000/ (localhost)
