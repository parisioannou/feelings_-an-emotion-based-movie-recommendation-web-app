To run the application:

1) Install all dependencies in requirements.txt.
2)In Command Window:
	a) activate environment where dependencies have been isntalled into.
	b) go into the project folder (djnago)
	c) run the command: python manage.py runserver

Make sure your camera is connected.
Visit URL: http://127.0.0.1:8000/ (localhost)