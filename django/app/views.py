#Import Libraries 
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http.response import StreamingHttpResponse
from django.conf import settings
import tensorflow as tf
from tensorflow.keras.models import load_model
global graph, model
import pandas as pd
import numpy as np
from .camera import VideoCamera
import cv2
import base64



###TENSORFLOW GPU OPERATIONS###
#Fraction of the available GPU memory to allocate for each process.
config = tf.compat.v1.ConfigProto(gpu_options =
                         tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.8))
config.gpu_options.allow_growth = True
#Environment to set and run TensorFlow operations
session = tf.compat.v1.Session(config=config)
tf.compat.v1.keras.backend.set_session(session)



###LOAD DEEP LEARNING MODELS###
#Face Detection Model
face_cascade = cv2.CascadeClassifier(f"{settings.STATICFILES_DIRS[0]}/models/haarcascade_frontalface_default.xml")
#Emotion Classification Model
model = load_model(settings.STATICFILES_DIRS[0] + "/models/FERmodel.h5")

###LOAD DATASETS###
#Load MovieLens Dataset (includes movie title-genre-rating correlation)
df = pd.read_csv(f"{settings.STATICFILES_DIRS[0]}/movies_metadata.csv")
df['genres'] = df['genres'].apply(lambda x: [i['name'] for i in eval(x)])


#Initialize Labels/Classes for emotion classifier
classes = {
	"0": "Angry",
	"1": "Happy",
	"2": "Sad",
	"3": "Neutral"
}

#Initialize emotion-movie genre mappings
emotion_genre_map = {
	"Angry": ["Action", "Adventure", "Crime", "Thriller"],
	"Happy": ["Comedy", "Family", "Mystery"],
	"Sad": ["Romance", "Music", "Science Fiction"],
	"Neutral": ["Documentary", "History", "Animation"]
}






#-----###MAIN FUNCTIONS###-----#



#Django-Python manipulation, to open page upon url call.#

def index(request):
	"""Opens Home Page when url is requested."""
	context={'a':1}
	return render(request,'app/index.html',context)


def movies(request):
	"""Opens Browse Movies Page when url is requested."""
	return render(request, 'app/movies.html')


def camera_page(request):
	"""Opens Recommend Movies Page when url is requested."""
	context={'a':1}
	return render(request, 'app/camera_page.html',context)




#CAMERA MANIPULATION#

def display_camera_frame(request):
	"""Opens and Displays Camera after request from camera_page.html page"""
	camera = VideoCamera()
	response = StreamingHttpResponse(start_camera(camera), content_type='multipart/x-mixed-replace; boundary=frame')
	return response


def start_camera(camera):
	"""Captures frame/image from Camera."""
	frame_index = 0
	while True:
		frame = camera.get_frame()
		if frame_index%10 == 0:
			with open(f"{settings.STATICFILES_DIRS[0]}/images/face", "wb+") as f:
				f.write(frame)
			f.close()
		frame_index+=1
		yield (b'--frame\r\n'
			   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


def ndarray_to_b64(ndarray):
    """Converts an np ndarray to a binary string readable by html-img tags."""
    img = cv2.cvtColor(ndarray, cv2.COLOR_RGB2BGR)
    _, buffer = cv2.imencode(".png", img)
    return base64.b64encode(buffer).decode("utf-8")




#Emotion Classification/Movie recommendation Functions


def get_emotion_and_images(request):
	"""Returns Image with detected face and movie recommendation results.""" 
	print(f"{settings.STATICFILES_DIRS[0]}/models/haarcascade_frontalface_default.xml")
	fd = open(f'{settings.STATICFILES_DIRS[0]}/images/face', "rb")
	img_str = fd.read()
	fd.close()

	nparr = np.frombuffer(img_str, np.uint8)
	image = cv2.imdecode(nparr, cv2.IMREAD_COLOR) 
	image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

	image, emotion = get_image_frame_and_emotion(image)

	recommended_movies = get_recommended_movies(emotion)

	context={"image_src":f"data:image/png;base64,{ndarray_to_b64(image)}",
			 "recommended_movies": recommended_movies}

	return render(request, 'app/emotion_and_movies.html', context)



def get_image_frame_and_emotion(image):
	"""Detects user's face in captured image and predicts the emotion. Then draws both in the image.""" 
	width = image.shape[0]
	height = image.shape[1]

	#gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	detect_face = face_cascade.detectMultiScale(image, scaleFactor=1.1, minNeighbors=6, minSize=(197, 197),
												flags=cv2.CASCADE_SCALE_IMAGE)
	for (x, y, w, h) in detect_face:
		input_img = np.expand_dims(cv2.resize(image[x:x+w, y:y+h], (197,197)), 0)
		input_img = input_img/255
		predictions = model.predict(input_img)
		emotion = classes[str(np.where(predictions == np.max(predictions))[1][0])]

		cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 2)
		cv2.putText(image, emotion, (x, y+h + 30), cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 0), 2)
		break

	return image, emotion



def get_recommended_movies(emotion):
	"""Chooses the 20 top rated movies in the specific genre according to emotion-genre mapping. 
	It randomly suggests 5 out of 20 each time.""" 
	movies = list(df[df["genres"].apply(lambda x: movie_recommended(x, emotion))].sort_values("vote_average",
																							  ascending=False).iloc[
				  :20].sample(5)["original_title"])
	return movies



def movie_recommended(movie_genres, emotion):
	"""Confirm Genres according to emotion-genre mapping.""" 
	for genre in emotion_genre_map[emotion]:
		if genre in movie_genres:
			return True
	return False





