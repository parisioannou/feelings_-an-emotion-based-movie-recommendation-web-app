"""URL patterns for main app"""

#Import Libraries/Modules
from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings


app_name = 'app'

urlpatterns = [
	#Home Page
	path('', views.index, name='index'),
	#Movies Page
	path('movies', views.movies,  name='movies'),
	#Recommend Movies Page
	path('camera_page', views.camera_page,  name='camera_page'),
	#Open Camera Function
    path('display_camera_frame/', views.display_camera_frame, name='display_camera_frame'),
    #Run Main Website Function and return results in emotion_and_movies.html page
	path('get_emotion_and_images/', views.get_emotion_and_images, name='get_emotion_and_images')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) #Include url to static folder where all  additional files are stored

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) #Url to Media folder (future addition)